const { Router } = require('express');

const router = Router();
const { getStudents, createStudent, getStudentById, deleteStudent, updateStudent } = require('../controller/alumno.contoller');
const { getTeachers, createTeacher, getTeacherbyId, updateTeacher, deleteTeacher } = require('../controller/profesor.controller');
const { getSubjects, createSubject, getSubjectById, deleteSubject, updateSubject } = require('../controller/materia.controller')

router.get('/alumnos', getStudents);
router.get('/alumnos/:id', getStudentById);
router.post('/alumnos', createStudent);
router.delete('/alumnos/:id', deleteStudent);
router.put('/alumnos/:id', updateStudent);

router.get('/profesores', getTeachers);
router.post('/profesores', createTeacher);
router.get('/profesores/:id', getTeacherbyId);
router.put('/profesores/:id', updateTeacher);
router.delete('/profesores/:id', deleteTeacher);

router.get('/materias', getSubjects);
router.post('/materias', createSubject);
router.get('/materias/:id', getSubjectById);
router.delete('/materias/:id', deleteSubject);
router.put('/materias/:id', updateSubject);
module.exports = router;