const { pool } = require('../connection/connection');


const getTeachers = async (req, res) =>{
    const response = await pool.query('SELECT * FROM profesores');
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getTeacherbyId = async(req, res) => {
   const response = await pool.query('SELECT * FROM profesores where cedula_profesor= $1', [req.params.id]);
   res.status(200).json(response.rows);
}

const createTeacher = async (req, res) => {
    const {cedula_profesor, nombre_profesor, apellido_profesor, edad_profesor, email_profesor } = req.body;
    const response = await pool.query('INSERT INTO profesores(cedula_profesor, nombre_profesor, apellido_profesor, edad_profesor, email_profesor) VALUES ($1, $2, $3, $4, $5)', [cedula_profesor, nombre_profesor, apellido_profesor, edad_profesor, email_profesor]);
    console.log(response.rowCount);
    res.json({
        message: 'Profesor agregado correctamente',
        body: {
            profesor: {
                cedula: cedula_profesor,
                nombres: nombre_profesor,
                apellidos: apellido_profesor,
                email: email_profesor,
                
            }
        }
    });
}


const deleteTeacher = async (req, res) => {
    const response = await pool.query('DELETE FROM profesores where cedula_profesor=$1', [req.params.id]);
    res.json({
        message: "Profesor eliminado correctamente",
        profesor:{
            cedula_profesor: req.params.id
        }
    })

    console.log(response);

}

const updateTeacher = async (req, res) => {
    const {nombre_profesor, apellido_profesor, edad_profesor, email_profesor} = req.body;
    const response = await pool.query('UPDATE profesores SET nombre_profesor = $1, apellido_profesor = $2, edad_profesor = $3, email_profesor = $4 where cedula_profesor = $5', [nombre_profesor, apellido_profesor, edad_profesor, email_profesor, req.params.id]);
    res.json({
        message: "Profesor actualizado correctamente",
        alumno:{
            cedula: req.params.id,
            nombres: nombre_profesor,
            apellidos: apellido_profesor,
            email: email_profesor
        }
    })
}
module.exports = {
  getTeachers,
  createTeacher,
  getTeacherbyId,
  updateTeacher,
  deleteTeacher
}