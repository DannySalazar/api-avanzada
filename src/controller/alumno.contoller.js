const { pool } = require('../connection/connection');
const getStudents = async (req, res) =>{
    const response = await pool.query('SELECT * FROM ALUMNOS');
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getStudentById = async(req, res) => {
   const response = await pool.query('SELECT * FROM alumnos where codigo= $1', [req.params.id]);
   res.status(200).json(response.rows);
}

const createStudent = async (req, res) => {
    const {codigo, nombres, apellidos, edad, email } = req.body;
    const response = await pool.query('INSERT INTO alumnos (codigo, nombres, apellidos, edad, email) VALUES($1, $2, $3, $4, $5)', [codigo, nombres, apellidos, edad, email]);
    console.log(response.rowCount);
    res.json({
        message: 'Alumno agregado correctamente',
        body: {
            alumno: {
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                email: req.body.email
                
            }
        }
    });
}


const deleteStudent = async (req, res) => {
    const response = await pool.query('DELETE FROM alumnos where codigo=$1', [req.params.id]);
    res.json({
        message: "Alumno eliminado correctamente",
        alumno:{
            codigo: req.params.id
        }
    })

    console.log(response);

}

const updateStudent = async (req, res) => {
    const {nombres, apellidos, edad, email} = req.body;
    const response = await pool.query('UPDATE alumnos SET nombres = $1, apellidos = $2, edad = $3, email = $4 where codigo = $5', [nombres, apellidos, edad, email, req.params.id]);
    res.json({
        message: "Alumno actualizado correctamente",
        alumno:{
            codigo: req.params.id,
            nombres: nombres,
            apellidos: apellidos,
            email: email
        }
    })
}
module.exports = {
    getStudents,
    getStudentById,
    createStudent,
    deleteStudent,
    updateStudent
}