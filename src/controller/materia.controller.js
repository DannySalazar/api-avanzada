const { pool } = require('../connection/connection');


const getSubjects = async (req, res) =>{
    const response = await pool.query('SELECT * FROM materias');
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getSubjectById = async(req, res) => {
   const response = await pool.query('SELECT * FROM materias where codigo_materia= $1', [req.params.id]);
   res.status(200).json(response.rows);
}

const createSubject = async (req, res) => {
    const {nombre_materia, profesor } = req.body;
    const response = await pool.query('INSERT INTO materias (nombre_materia, profesor) VALUES ($1, $2)', [nombre_materia, profesor]);
    console.log(response);
    res.json({
        message: 'Materia agregada correctamente',
        body: {
            materia: {
                nombre_materia: nombre_materia,
                profesor: profesor
                
            }
        }
    });
}


const deleteSubject = async (req, res) => {
    const response = await pool.query('DELETE FROM materias where codigo_materia=$1', [req.params.id]);
    res.json({
        message: "Materia eliminada correctamente",
        materia:{
            codigo_materia: req.params.id
        }
    })

    console.log(response);

}

const updateSubject = async (req, res) => {
    const { nombre_materia, profesor } = req.body;
    const response = await pool.query('UPDATE materias SET nombre_materia = $1, profesor = $2 where codigo_materia=$3', [nombre_materia, profesor, req.params.id]);
    res.json({
        message: "Materia actualizada correctamente",
        alumno:{
            codigo_materia: req.params.id,
            nombre_materia: nombre_materia,
            profesor: profesor
        }
    })
}
module.exports = {
  getSubjects,
  createSubject,
  getSubjectById,
  deleteSubject,
  updateSubject

}