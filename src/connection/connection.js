const {Pool} = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: 'velasco123',
    database: 'school',
    port: '5432'
})

module.exports = {
    pool
}