--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alumnos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alumnos (
    codigo character varying(15) NOT NULL,
    nombres text,
    apellidos text,
    edad integer,
    email text
);


ALTER TABLE public.alumnos OWNER TO postgres;

--
-- Name: materias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materias (
    codigo_materia integer NOT NULL,
    nombre_materia text,
    profesor text
);


ALTER TABLE public.materias OWNER TO postgres;

--
-- Name: materias_codigo_materia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.materias_codigo_materia_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.materias_codigo_materia_seq OWNER TO postgres;

--
-- Name: materias_codigo_materia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.materias_codigo_materia_seq OWNED BY public.materias.codigo_materia;


--
-- Name: profesores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profesores (
    cedula_profesor text NOT NULL,
    nombre_profesor text,
    apellido_profesor text,
    edad_profesor integer,
    email_profesor text
);


ALTER TABLE public.profesores OWNER TO postgres;

--
-- Name: materias codigo_materia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materias ALTER COLUMN codigo_materia SET DEFAULT nextval('public.materias_codigo_materia_seq'::regclass);


--
-- Data for Name: alumnos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alumnos (codigo, nombres, apellidos, edad, email) FROM stdin;
213445012	Sebastian Andres	Diaz Perez	21	sebas_diaz@gmail.com
21233412	Diana	Garcia	20	dgarcia@gmail.com
213998992	Carlos Andres	Alvarado Velez	23	alvaradocarlos@gmail.com
\.


--
-- Data for Name: materias; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materias (codigo_materia, nombre_materia, profesor) FROM stdin;
2	Calculo Integral	1089788987
5	Bases de Datos	59702336
6	Calculo Multivariable	1089788987
\.


--
-- Data for Name: profesores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profesores (cedula_profesor, nombre_profesor, apellido_profesor, edad_profesor, email_profesor) FROM stdin;
1089788987	Fernando	Estrada	49	fer_estrada@outlook.es
59702336	Sandra Marcela	Zarama Silva	36	sanzarama@outlook.es
108945567	Javier	Diaz	55	javidiaz@gmail.com
\.


--
-- Name: materias_codigo_materia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.materias_codigo_materia_seq', 6, true);


--
-- Name: alumnos alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alumnos
    ADD CONSTRAINT alumnos_pkey PRIMARY KEY (codigo);


--
-- Name: materias materias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materias
    ADD CONSTRAINT materias_pkey PRIMARY KEY (codigo_materia);


--
-- Name: profesores profesores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesores
    ADD CONSTRAINT profesores_pkey PRIMARY KEY (cedula_profesor);


--
-- Name: materias materias_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materias
    ADD CONSTRAINT materias_profesor_fkey FOREIGN KEY (profesor) REFERENCES public.profesores(cedula_profesor);


--
-- PostgreSQL database dump complete
--

